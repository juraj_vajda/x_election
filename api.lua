local storage = minetest.get_mod_storage()
local save_timer_handle = 0

--
-- settings, numbers are in seconds
--

-- how often are we going to save application state to file
local save_timer = 30
-- period length for registering
local register_timer = 15
-- period length for voting
local voting_timer = 15
-- period length for elected players
local election_timer = 60
-- minimum players to be registered for elections in order to proceed to voting period
local min_registered_to_vote = 1
-- local min_registered_to_vote = 5
local first_privs = { "basic_privs", "fly", "fast", "kick", "xban" }
local second_privs = { "basic_privs", "fly", "kick" }
local third_privs = { "basic_privs", "kick" }

--
-- definitions made by this mod that other mods can use too
--

x_elections = {}
-- data what will be saved to file
x_elections.data = {}
-- @TODO: do we need this in data ?
x_elections.data.selected_candidate = {}
-- registered players for elections
x_elections.data.registered = {}
-- players what have already voted (player can vote only once)
x_elections.data.already_voted = {}
-- elected player (top 3)
x_elections.data.elected = {}
-- is the election/mod running for the first time ?
x_elections.data.first_run = true
-- keep timer value always saved
x_elections.data.timer_handle = 0

-- state management variables
x_elections.data.start_register = true
x_elections.data.start_voting = false
x_elections.data.start_elections = false
x_elections.data.current_period = "register"

function x_elections:sync_storage()
	local storage_tbl = storage:to_table().fields.data
	storage_tbl = minetest.deserialize(storage_tbl) or {}
	print("------------storage_tbl-----------")
	print(dump(storage_tbl))
	print("-----------/storage_tbl-----------")

	x_elections.data.registered = storage_tbl["registered"] or x_elections.data.registered
	x_elections.data.already_voted = storage_tbl["already_voted"] or x_elections.data.already_voted
	x_elections.data.selected_candidate = storage_tbl["selected_candidate"] or x_elections.data.selected_candidate
	x_elections.data.elected = storage_tbl["elected"] or x_elections.data.elected
	x_elections.data.timer_handle = storage_tbl["timer_handle"] or x_elections.data.timer_handle
	x_elections.data.current_period = storage_tbl["current_period"] or x_elections.data.current_period

	if storage_tbl["start_register"] ~= nil then
		x_elections.data.start_register = storage_tbl["start_register"]
	end

	if storage_tbl["start_voting"] ~= nil then
		x_elections.data.start_voting = storage_tbl["start_voting"]
	end

	if storage_tbl["start_elections"] ~= nil then
		x_elections.data.start_elections = storage_tbl["start_elections"]
	end

	if storage_tbl["first_run"] ~= nil then
		x_elections.data.first_run = storage_tbl["first_run"]
	end

	print("------------x_elections-----------")
	print(dump(x_elections.data))
	print("-----------/x_elections-----------")
	x_elections:save()

	---
	-- local sorted_cadidates_tbl = get_sorted_cadidates(x_elections.data.registered)

	-- for k, v in ipairs(sorted_cadidates_tbl) do
	-- 	print(v["player_name"], v["votes"])
	-- end

	-- print("#1", dump(sorted_cadidates_tbl[1]))
	-- print("#2", dump(sorted_cadidates_tbl[2]))
	-- print("#3", dump(sorted_cadidates_tbl[3]))
	---
end

function x_elections:save()
	local datastring = minetest.serialize(self.data)
	if not datastring then
		return
	end

	storage:set_string("data", datastring)

	print("================= saved data ======================")
	print(datastring)
	print("===================================================")
end

function x_elections:get_formspec(formtype, params)
	local formspec = ""
	local params = params or {}

	-- REGISTER
	if formtype == "register" then
		local playername = params.player:get_player_name()
		local register_will_start = params.register_will_start or 0
		local register_will_end = params.register_will_end or 0

		local timer_label = ""

		if register_will_start > 0 then
			timer_label = "Register period will start in: "..register_will_start.." seconds"

		elseif register_will_end > 0 then
			timer_label = "Register period will end in: "..register_will_end.." seconds"

		else
			timer_label = "Registering period is closed. The active period now is: "..x_elections.data.current_period
		end

		if self.data.current_period ~= "register" then
		-- register period is closed
			formspec = "size[8,9]"..
				default.gui_bg..
				default.gui_bg_img..
				default.gui_slots..
				"label[0.5,0.6;"..timer_label.."]"..
				"button_exit[0.5,8;2,1.5;button;Cancel]"

		elseif not self.data.registered[playername] then
		-- not registered yet
			formspec = "size[8,9]"..
				default.gui_bg..
				default.gui_bg_img..
				default.gui_slots..
				"textarea[0.8,0.5;7,3.5;x_elections:register_01_desc;Hi "..playername..",;\nHere you can register your self as a candidate for upcoming elections.]"..
				"textarea[0.8,4;7,3.5;x_elections:register_01;Write about why should people vote for you:;]"..
				"label[0.5,7.7.5;"..timer_label.."]"..
				"button_exit[0.5,8;2,1.5;button;Cancel]"..
				"button_exit[4.5,8;3,1.5;button;REGISTER ME]"

		else
		-- already registered
			formspec = "size[8,9]"..
				default.gui_bg..
				default.gui_bg_img..
				default.gui_slots..
				"label[0.5,0.6;"..timer_label.."]"..
				"textarea[0.8,1.5;7,3.5;x_elections:register_01_desc;\nYou have been already registered for next elections!;\nIf you wish to remove your candidacy you can remove your registration by clicking the 'remove' button below.\n\nWARNING: Removing your account will reset your current votes to zero!]"..
				"button_exit[0.5,8;2,1.5;button;Cancel]"..
				"button_exit[4.5,8;3,1.5;button;REMOVE ACCOUNT]"
		end

	-- VOTE
	elseif formtype == "vote" then
		local playername = params.player:get_player_name()
		local i = 0
		local textlist = ""
		local index = params.index or 1
		local registered_tbl_length = self.get_table_length(self.data.registered)
		local registered_tbl_indexed = {}
		local registered_tbl_indexed_candidates = {}

		-- build textlist
		for key, val in pairs(self.data.registered) do
			i = i + 1
			print(key.." : "..dump(val))

			table.insert(registered_tbl_indexed, val)
			table.insert(registered_tbl_indexed_candidates, key)

			if i == registered_tbl_length then
			-- no comma for the last one
				textlist = textlist..key
			else
				textlist = textlist..key..","
			end
		end

		local vote_btn = "button_exit[6,7.5;2.4,1.5;button;VOTE]"
		local label_already_voted = ""

		-- don't show VOTE button when already voted
		if self.data.already_voted[playername] then
			vote_btn = ""
			label_already_voted = "label[6,7.5;You have already voted.]"
		end

		-- check if there is someone already registered
		local candidate_desc = "no cadidates were registered yet"

		if #registered_tbl_indexed > 0 then
			candidate_desc = registered_tbl_indexed[index].desc

			if candidate_desc == "" then
				candidate_desc = "\nThis candidate didn't wrote any description about why you should be voting for him."
			end

			self.data.selected_candidate = registered_tbl_indexed_candidates[index]
		else
			vote_btn = ""
		end

		-- show timer label
		local vote_will_start = params.vote_will_start or 0
		local vote_will_end = params.vote_will_end or 0

		local timer_label = ""

		if vote_will_start > 0 then
			vote_btn = ""
			label_already_voted = "label[4.2,7.2;Vote period will start in: "..vote_will_start.." seconds]"

		elseif vote_will_end > 0 then
			timer_label = "label[4.2,7.2;Vote period will end in: "..vote_will_end.." seconds]"

		else
			vote_btn = ""
			label_already_voted = "label[4.2,7.2;Vote period is closed. The active period now is: "..x_elections.data.current_period.."]"
		end

		formspec = "size[10,8.5]"..
			default.gui_bg..
			default.gui_bg_img..
			default.gui_slots..
			"label[0,0;Candidates:]"..
			"textlist[0,0.5;4,8;candidates_list;"..textlist..";"..index..";false]"..
			"textarea[4.5,1.5;5.8,5;x_elections:vote;Why you should vote for this candidate:;\n"..candidate_desc.."]"..
			"button_exit[9,0;1,1;button;X]"..
			timer_label..
			label_already_voted..
			vote_btn
	end

	return formspec
end

minetest.register_on_player_receive_fields(function(player, formname, fields)
	local playername = player:get_player_name()
	print(formname)
	print(dump(storage:to_table()))
	print(dump(fields))

	-- REGISTER
	if formname == "x_elections:formspec_register" then
		if fields.button == "REGISTER ME" then
			-- save candidate to list
			x_elections.data.registered[playername] = {
				desc = fields["x_elections:register_01"],
				votes = 0
			}
			x_elections:save()

		elseif fields.button == "REMOVE ACCOUNT" then
			x_elections.data.registered[playername] = nil
			x_elections:save()
		end

	-- VOTE
	elseif formname == "x_elections:formspec_vote" then
		local event = minetest.explode_textlist_event(fields["candidates_list"])

		if event.type == "CHG" then
			local vote_will_start, vote_will_end = x_elections.get_timings("vote")

			local index = event.index
			local formspec = x_elections:get_formspec("vote", { index = index, player = player, vote_will_start = vote_will_start, vote_will_end = vote_will_end })

			minetest.show_formspec(playername, "x_elections:formspec_vote", formspec)

		elseif fields.button == "VOTE" then
			if x_elections.data.registered[x_elections.data.selected_candidate].votes == nil then
				x_elections.data.registered[x_elections.data.selected_candidate].votes = 1
			else
				x_elections.data.registered[x_elections.data.selected_candidate].votes = x_elections.data.registered[x_elections.data.selected_candidate].votes + 1
			end

			x_elections.data.already_voted[playername] = true
			x_elections:save()

			print(dump(x_elections.data.already_voted))
			print(dump(x_elections.data.registered))
		end

		print(dump(event))
	end
end)

function x_elections.on_rightclick(pos, node, clicker, itemstack, pointed_thing)
	local clickername = clicker:get_player_name()
	local nodename = node.name
	local nodename_split = nodename:split(":")
	print("=======================================")
	print(dump(nodename))
	print(dump(clickername))
	print("=======================================")

	-- REGISTER
	if nodename == "x_elections:register" then
		print(x_elections.data.current_period)
		local register_will_start, register_will_end = x_elections.get_timings("register")

		local formspec = x_elections:get_formspec("register", { player = clicker, register_will_start = register_will_start, register_will_end = register_will_end })
		minetest.show_formspec(clickername, "x_elections:formspec_"..nodename_split[2], formspec)

	-- VOTE
	elseif nodename == "x_elections:vote" then
		local vote_will_start, vote_will_end = x_elections.get_timings("vote")

		local formspec = x_elections:get_formspec("vote", { player = clicker, vote_will_start = vote_will_start, vote_will_end = vote_will_end })
		minetest.show_formspec(clickername, "x_elections:formspec_"..nodename_split[2], formspec)

	-- INFO
	elseif nodename == "x_elections:info" then

	end
end

function x_elections.after_place_node(pos, placer, itemstack, pointed_thing)
	local meta = minetest.get_meta(pos)
	local nodename = itemstack:get_name()
	local nodename_split = nodename:split(":")
	meta:set_string("infotext", "elections: "..nodename_split[2]);
end

function x_elections:grant_players()
	print("=== electing players ===")

	local sorted_cadidates_tbl = get_sorted_cadidates(self.data.registered)

	print("#1", dump(sorted_cadidates_tbl[1]))
	-- print("#2", dump(sorted_cadidates_tbl[2]))
	-- print("#3", dump(sorted_cadidates_tbl[3]))

	self.data.elected["first"] = sorted_cadidates_tbl[1]
	-- self.data.elected["second"] = sorted_cadidates_tbl[2]
	-- self.data.elected["third"] = sorted_cadidates_tbl[3]

	print("elected", dump(self.data.elected))

	minetest.set_player_privs(sorted_cadidates_tbl[1]["player_name"], {fly = true, fast = true})
end

function x_elections:revoke_players()
	print("=== revoke players ===")

	if self.data.elected["first"] then
		local player_privs = minetest.get_player_privs(self.data.elected["first"]["player_name"])

		print(self.data.elected["first"]["player_name"], dump(player_privs))

		player_privs.fly = nil
		player_privs.fast = nil

		minetest.set_player_privs(self.data.elected["first"]["player_name"], player_privs)
	end

	-- testing
	if self.data.elected["first"]["player_name"] ~= "tester_02" then
		self.data.registered["tester_02"].votes = 100
		self.data.registered["tester_01"].votes = 1
	else
		self.data.registered["tester_01"].votes = 100
		self.data.registered["tester_02"].votes = 1
	end
end

function x_elections.get_timings(period)
	-- REGISTER TIMINGS
	if period == "register" then
		local register_will_start = 0
		local register_will_end = 0

		-- count when new register period will end
		if x_elections.data.current_period == "register" then
			register_will_end = math.floor(election_timer - voting_timer - x_elections.data.timer_handle)

			if register_will_start ~= 0 then
				register_will_start = 0
			end

			print("register period will end in: ", math.floor(election_timer - voting_timer - x_elections.data.timer_handle))

		-- count when new register period will start
		else
			register_will_start = math.floor(election_timer - (register_timer + voting_timer) - x_elections.data.timer_handle)

			if register_will_end ~= 0 then
				register_will_end = 0
			end

			print("register period will start in: ", math.floor(election_timer - (register_timer + voting_timer) - x_elections.data.timer_handle))
		end

		return register_will_start, register_will_end

	-- VOTE TIMINGS
	elseif period == "vote" then
		local vote_will_start = 0
		local vote_will_end = 0

		-- count when new vote period will end
		if x_elections.data.current_period == "vote" then
			vote_will_end = math.floor(election_timer - x_elections.data.timer_handle)

			if vote_will_start ~= 0 then
				vote_will_start = 0
			end

			print("vote period will end in: ", math.floor(election_timer - x_elections.data.timer_handle))

		-- count when new vote period will start
		else
			vote_will_start = math.floor(election_timer - voting_timer - x_elections.data.timer_handle)

			if vote_will_end ~= 0 then
				vote_will_end = 0
			end

			print("vote period will start in: ", math.floor(election_timer - voting_timer - x_elections.data.timer_handle))
		end

		return vote_will_start, vote_will_end
	end
end

-- @param {table} ass_tbl - associative array
-- @return {table} temp_tbl - indexed array
function get_sorted_cadidates(ass_tbl)
	local temp_tbl = {}
	for k,v in pairs(ass_tbl) do
		v["player_name"] = k
		table.insert(temp_tbl, v)
	end

	table.sort(temp_tbl, function(a, b) return a.votes > b.votes end)

	return temp_tbl
end

function x_elections.get_table_length(tbl)
	local length = 0
	for k, v in pairs(tbl) do
		length = length + 1
	end
	return length
end

--
-- Global step
--

minetest.register_globalstep(function(dtime)
	save_timer_handle = save_timer_handle + dtime

	if x_elections.data.first_run then
		x_elections.data.timer_handle = election_timer - (register_timer + voting_timer)
		x_elections.data.first_run = false
		x_elections:save()
	else
		x_elections.data.timer_handle = x_elections.data.timer_handle + dtime
	end

	print(x_elections.data.timer_handle)

	-- REGISTER
	if x_elections.data.timer_handle >= election_timer - (register_timer + voting_timer) then

		if x_elections.data.start_register then
			x_elections.data.start_register = false
			x_elections.data.start_voting = true
			x_elections.data.current_period = "register"
			x_elections:save()
			print("ELECTIONS: REGISTER PERIOD HAS STARTED.")
		end
	end

	-- VOTE
	if x_elections.data.timer_handle >= election_timer - voting_timer then

		if x_elections.data.start_voting then
			print("ELECTIONS: REGISTER PERIOD HAS ENDED.")

			-- repeat the registered cycle when there is not enough registered player to continue
			if x_elections.get_table_length(x_elections.data.registered) < min_registered_to_vote then

				print("NOT ENOUGH REGISTERED PLAYERS")
				x_elections.data.first_run = true
				return
			end

			x_elections.data.start_voting = false
			x_elections.data.start_elections = true
			x_elections.data.current_period = "vote"
			x_elections:save()

			print("ELECTIONS: VOTING PERIOD HAS STARTED.")
		end
	end

	-- ELECTED
	if x_elections.data.timer_handle >= election_timer then

		if x_elections.data.start_elections then
			x_elections.data.start_elections = false
			x_elections.data.start_register = true
			x_elections.data.current_period = "elections"
			x_elections.data.timer_handle = 0
			x_elections:save()

			x_elections:revoke_players()
			x_elections:grant_players()
			print("ELECTIONS: VOTING PERIOD HAS ENDED.")
			print("ELECTIONS: NEW ELECTION PERIOD HAS STARTED.")
		end
	end

	-- save data
	if save_timer_handle >= save_timer then
		print("saving data")
		x_elections:save()
		save_timer_handle = 0
	end
end)

minetest.register_on_shutdown(function()
	x_elections:save()
end)