-- Minetest Survival X mod: x_elections
-- See README.txt for licensing and other information.

-- Load files
local default_path = minetest.get_modpath("x_elections")

dofile(default_path.."/api.lua")
dofile(default_path.."/nodes.lua")

x_elections.sync_storage()