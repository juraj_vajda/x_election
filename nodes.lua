-- register block
minetest.register_node("x_elections:register", {
	description = "Register for elections.",
	tiles = {
		"x_elections_register_block.png",
		"x_elections_register_block.png",
		"x_elections_register_block_side.png",
		"x_elections_register_block_side.png",
		"x_elections_register_block_side.png",
		"x_elections_register_block_side.png"
	},
	is_ground_content = false,
	groups = {cracky = 2, stone = 1},
	sounds = default.node_sound_stone_defaults(),
	paramtype = "light",
	light_source = 4,
	after_place_node = x_elections.after_place_node,
	on_rightclick = x_elections.on_rightclick,
	on_timer = x_elections.on_timer_register,
})

-- register block
minetest.register_node("x_elections:vote", {
	description = "Vote for elections.",
	tiles = {
		"x_elections_vote_block.png",
		"x_elections_vote_block.png",
		"x_elections_vote_block_side.png",
		"x_elections_vote_block_side.png",
		"x_elections_vote_block_side.png",
		"x_elections_vote_block_side.png"
	},
	is_ground_content = false,
	groups = {cracky = 2, stone = 1},
	sounds = default.node_sound_stone_defaults(),
	paramtype = "light",
	light_source = 4,
	after_place_node = x_elections.after_place_node,
	on_rightclick = x_elections.on_rightclick,
})

-- register block
minetest.register_node("x_elections:info", {
	description = "Informations about elections.",
	tiles = {
		"x_elections_info_block.png",
		"x_elections_info_block.png",
		"x_elections_info_block_side.png",
		"x_elections_info_block_side.png",
		"x_elections_info_block_side.png",
		"x_elections_info_block_side.png"
	},
	is_ground_content = false,
	groups = {cracky = 2, stone = 1},
	sounds = default.node_sound_stone_defaults(),
	paramtype = "light",
	light_source = 4,
	after_place_node = x_elections.after_place_node,
	on_rightclick = x_elections.on_rightclick,
})